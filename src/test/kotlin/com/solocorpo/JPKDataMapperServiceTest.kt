package com.solocorpo

import com.solocorpo.data.JPKData
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import org.junit.Test
import java.io.StringReader
import java.io.StringWriter
import javax.xml.transform.OutputKeys
import javax.xml.transform.Source
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import javax.xml.transform.stream.StreamSource


class JPKDataMapperServiceTest {
    val data = "{\n" +
            "  \"expensedProductInvoices\": [\n" +
            "    {\n" +
            "      \"OPIS\": \"orange\",\n" +
            "      \"NIP\": \"5260250995\",\n" +
            "      \"NAZWA\": \"ORANGE POLSKA SPÓŁKA AKCYJNA\",\n" +
            "      \"ADRES_DOSTAWY\": \"02-326 Warszawa, Aleje Jerozolimskie 160\",\n" +
            "      \"DATA_WYSTAWIENIA\": \"2020-02-14T00:00:00.000Z\",\n" +
            "      \"NR_DOKUMENTU\": \"20020450381591\",\n" +
            "      \"NETTO\": 32.17,\n" +
            "      \"VAT\": 7.4,\n" +
            "      \"SUMA_Z_VAT\": 39.57\n" +
            "    }\n" +
            "  ],\n" +
            "  \"expensedCarInvoices\": [\n" +
            "    {\n" +
            "      \"OPIS\": \"paliwo\",\n" +
            "      \"NIP\": \"7740001454\",\n" +
            "      \"NAZWA\": \"POLSKI KONCERN NAFTOWY ORLEN SPÓŁKA AKCYJNA\",\n" +
            "      \"ADRES_DOSTAWY\": \"09-411 Płock, ul. Chemików 7\",\n" +
            "      \"DATA_WYSTAWIENIA\": \"2020-02-24T00:00:00.000Z\",\n" +
            "      \"NR_DOKUMENTU\": \"271K20/1427/20\",\n" +
            "      \"NETTO\": 191.24,\n" +
            "      \"50%_NETTO\": 95.62,\n" +
            "      \"VAT\": 21.99,\n" +
            "      \"75%_NETTO_+_50%_VAT\": 159.92\n" +
            "    },\n" +
            "    {\n" +
            "      \"OPIS\": \"paliwo\",\n" +
            "      \"NIP\": \"7740001454\",\n" +
            "      \"NAZWA\": \"POLSKI KONCERN NAFTOWY ORLEN SPÓŁKA AKCYJNA\",\n" +
            "      \"ADRES_DOSTAWY\": \"09-411 Płock, ul. Chemików 7\",\n" +
            "      \"DATA_WYSTAWIENIA\": \"2020-02-15T00:00:00.000Z\",\n" +
            "      \"NR_DOKUMENTU\": \"223K20/1427/20\",\n" +
            "      \"NETTO\": 208,\n" +
            "      \"50%_NETTO\": 104,\n" +
            "      \"VAT\": 23.92,\n" +
            "      \"75%_NETTO_+_50%_VAT\": 173.94\n" +
            "    },\n" +
            "    {\n" +
            "      \"OPIS\": \"paliwo\",\n" +
            "      \"NIP\": \"9720865431\",\n" +
            "      \"NAZWA\": \"BP EUROPA SE SPÓŁKA EUROPEJSKA ODDZIAŁ W POLSCE\",\n" +
            "      \"ADRES_DOSTAWY\": \"Ul. Jasnogórska 1 31-358 Kraków\",\n" +
            "      \"DATA_WYSTAWIENIA\": \"2020-02-01T00:00:00.000Z\",\n" +
            "      \"NR_DOKUMENTU\": \"I20146B02000298\",\n" +
            "      \"NETTO\": 198.8,\n" +
            "      \"50%_NETTO\": 99.4,\n" +
            "      \"VAT\": 22.86,\n" +
            "      \"75%_NETTO_+_50%_VAT\": 166.25\n" +
            "    },\n" +
            "    {\n" +
            "      \"OPIS\": \"autostrada delegacja-02-2020#1\",\n" +
            "      \"NIP\": \"5851351185\",\n" +
            "      \"NAZWA\": \"BLUE MEDIA SPÓŁKA AKCYJNA\",\n" +
            "      \"ADRES_DOSTAWY\": \"81-718 Sopot, ul. Powstańców Warszawy 6\",\n" +
            "      \"DATA_WYSTAWIENIA\": \"2020-02-22T00:00:00.000Z\",\n" +
            "      \"NR_DOKUMENTU\": \"26842/2/2020/APT\",\n" +
            "      \"NETTO\": 5.69,\n" +
            "      \"50%_NETTO\": 2.845,\n" +
            "      \"VAT\": 0.65,\n" +
            "      \"75%_NETTO_+_50%_VAT\": 4.76\n" +
            "    },\n" +
            "    {\n" +
            "      \"OPIS\": \"autostrada delegacja-02-2020#1\",\n" +
            "      \"NIP\": \"5851351185\",\n" +
            "      \"NAZWA\": \"BLUE MEDIA SPÓŁKA AKCYJNA\",\n" +
            "      \"ADRES_DOSTAWY\": \"81-718 Sopot, ul. Powstańców Warszawy 6\",\n" +
            "      \"DATA_WYSTAWIENIA\": \"2020-02-22T00:00:00.000Z\",\n" +
            "      \"NR_DOKUMENTU\": \"26821/2/2020/APT\",\n" +
            "      \"NETTO\": 5.69,\n" +
            "      \"50%_NETTO\": 2.845,\n" +
            "      \"VAT\": 0.65,\n" +
            "      \"75%_NETTO_+_50%_VAT\": 4.76\n" +
            "    }\n" +
            "  ],\n" +
            "  \"vatIssuedInvoices\": [\n" +
            "    {\n" +
            "      \"OPIS\": \"sprzedaz test\",\n" +
            "      \"NIP\": \"1215455485\",\n" +
            "      \"NAZWA\": \"Company\",\n" +
            "      \"ADRES_DOSTAWY\": \"Company address\",\n" +
            "      \"DATA_WYSTAWIENIA\": \"2020-02-10T00:00:00.000Z\",\n" +
            "      \"NR_DOKUMENTU\": \"02/2020\",\n" +
            "      \"NETTO\": 10000,\n" +
            "      \"VAT\": 2300,\n" +
            "      \"SUMA_Z_VAT\": 12300\n" +
            "    }\n" +
            "  ],\n" +
            "  \"euIssuedInvoices\": [\n" +
            "    {\n" +
            "      \"OPIS\": \"usługi\",\n" +
            "      \"NIP\": \"1215455485\",\n" +
            "      \"KRAJ\": \"GB\",\n" +
            "      \"NAZWA\": \"Company\",\n" +
            "      \"ADRES_DOSTAWY\": \"Company address\",\n" +
            "      \"DATA_WYSTAWIENIA\": \"2020-02-29T00:00:00.000Z\",\n" +
            "      \"NR_DOKUMENTU\": \"02/2020\",\n" +
            "      \"KWOTA\": 1500000,\n" +
            "      \"WALUTA\": \"EUR\",\n" +
            "      \"KURS_Z\": \"2020-02-28T00:00:00.000Z\",\n" +
            "      \"KURS\": \"4.3355\",\n" +
            "      \"W_PLN\": 15000000.92\n" +
            "    }\n" +
            "  ],\n" +
            "  \"accountingPeriod\": \"2020-02-01T00:00:00.000Z\",\n" +
            "  \"currentPeriodDetails\": {\n" +
            "    \"previousPeriodVATOwned\": 67,\n" +
            "    \"nettoPLIncome\": 10000,\n" +
            "    \"vatOwned\": 2300,\n" +
            "    \"nettoEUIncome\": 15000000,\n" +
            "    \"nettoPLExpense\": 371,\n" +
            "    \"vatExpense\": 85,\n" +
            "    \"vatOwnedAllYearTotal\": 1500\n" +
            "  }\n" +
            "}\n"

    @Test
    fun mapData() {
        val jpkService = JPKDataMapperService()

        val json = Json(JsonConfiguration.Stable)
        val mapData = jpkService.convertToJPKFile(json.parse(JPKData.serializer(), data))
        val printXML = jpkService.printXML(mapData)

//        println(printXML)
    }
}