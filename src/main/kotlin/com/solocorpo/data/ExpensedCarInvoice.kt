package com.solocorpo.data

import kotlinx.serialization.Serializable

@Serializable
data class ExpensedCarInvoice(
    val `50%_NETTO`: Double,
    val `75%_NETTO_+_50%_VAT`: Double,
    val ADRES_DOSTAWY: String,
    val DATA_WYSTAWIENIA: String,
    val NAZWA: String,
    val NETTO: Double,
    val NIP: String,
    val NR_DOKUMENTU: String,
    val OPIS: String,
    val VAT: Double
)