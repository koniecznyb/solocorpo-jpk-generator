package com.solocorpo.data

import kotlinx.serialization.Serializable

@Serializable
data class VatIssuedInvoice(
    val ADRES_DOSTAWY: String,
    val DATA_WYSTAWIENIA: String,
    val NAZWA: String,
    val NETTO: Long,
    val NIP: String,
    val NR_DOKUMENTU: String,
    val OPIS: String,
    val SUMA_Z_VAT: Long,
    val VAT: Long
)