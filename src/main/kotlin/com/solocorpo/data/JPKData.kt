package com.solocorpo.data

import kotlinx.serialization.Serializable

@Serializable
data class JPKData(
    val accountingPeriod: String,
    val currentPeriodDetails: CurrentPeriodDetails,
    val euIssuedInvoices: List<EuIssuedInvoice>,
    val expensedCarInvoices: List<ExpensedCarInvoice>,
    val expensedProductInvoices: List<ExpensedProductInvoice>,
    val vatIssuedInvoices: List<VatIssuedInvoice>
)