package com.solocorpo.data

import kotlinx.serialization.Serializable

@Serializable
data class CurrentPeriodDetails(
    val nettoEUIncome: Long,
    val nettoPLExpense: Long,
    val nettoPLIncome: Long,
    val previousPeriodVATOwned: Long,
    val vatExpense: Long,
    val vatOwned: Long,
    val vatOwnedAllYearTotal: Long
)