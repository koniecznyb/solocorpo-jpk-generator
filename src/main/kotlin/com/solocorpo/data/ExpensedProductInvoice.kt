package com.solocorpo.data

import kotlinx.serialization.Serializable

@Serializable
data class ExpensedProductInvoice(
    val ADRES_DOSTAWY: String,
    val DATA_WYSTAWIENIA: String,
    val NAZWA: String,
    val NETTO: Double,
    val NIP: String,
    val NR_DOKUMENTU: String,
    val OPIS: String,
    val SUMA_Z_VAT: Double,
    val VAT: Double
)