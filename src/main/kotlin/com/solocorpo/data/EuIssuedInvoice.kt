package com.solocorpo.data

import kotlinx.serialization.Serializable

@Serializable
data class EuIssuedInvoice(
    val ADRES_DOSTAWY: String,
    val DATA_WYSTAWIENIA: String,
    val KRAJ: String,
    val KURS: String,
    val KURS_Z: String,
    val KWOTA: Long,
    val NAZWA: String,
    val NIP: String,
    val NR_DOKUMENTU: String,
    val OPIS: String,
    val WALUTA: String,
    val W_PLN: Double
)