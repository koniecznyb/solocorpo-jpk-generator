package com.solocorpo

import org.apache.kafka.clients.consumer.Consumer
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.serialization.StringDeserializer
import java.util.*

fun main(args: Array<String>) {
    val brokers: String = System.getenv("KAFKA_BROKERS") ?: "kafka";
    val kafkaService = KafkaService(createConsumer(brokers), true)
    kafkaService.consumeMessages(JPKDataMapperService())
}

fun createConsumer(brokers: String): Consumer<String, String> {
    val props = Properties()
    props["bootstrap.servers"] = brokers
    props["group.id"] = "file-generation-request-kotlin"
    props["key.deserializer"] = StringDeserializer::class.java
    props["value.deserializer"] = StringDeserializer::class.java
    val kafkaConsumer: Consumer<String, String> = KafkaConsumer(props)
    kafkaConsumer.subscribe(listOf("file-generation-request"))
    return kafkaConsumer
}




