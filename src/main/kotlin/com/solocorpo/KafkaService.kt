package com.solocorpo

import com.solocorpo.data.JPKData
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import org.apache.kafka.clients.consumer.Consumer
import java.time.Duration
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller


class KafkaService(
    private val consumer: Consumer<String, String>,
    private val infiniteLoop: Boolean
) {
    fun consumeMessages(jpkDataMapperService: JPKDataMapperService) {
        do {
            val records = consumer.poll(Duration.ofSeconds(1))
            records.iterator().forEach {
                val jpkData = it.value()

                val json = Json(JsonConfiguration.Stable)
                val jpkFile = jpkDataMapperService.convertToJPKFile(json.parse(JPKData.serializer(), jpkData))
                val jpkXML = jpkDataMapperService.printXML(jpkFile)

                println(jpkXML)

            }
        } while (infiniteLoop)
    }
}
