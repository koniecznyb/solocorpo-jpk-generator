//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.22 at 10:13:02 PM CET 
//


package com.solocorpo.xsd;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TDowoduZakupu.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TDowoduZakupu">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MK"/>
 *     &lt;enumeration value="VAT_RR"/>
 *     &lt;enumeration value="WEW"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TDowoduZakupu", namespace = "http://crd.gov.pl/wzor/2020/02/25/9142/")
@XmlEnum
public enum TDowoduZakupu {


    /**
     * MK - Faktura wystawiona przez podatnika b\u0119d\u0105cego dostawc\u0105 lub us\u0142ugodawc\u0105, kt�ry wybra\u0142 metod\u0119 kasow\u0105 rozlicze\u0144 okre\u015blon\u0105 w art. 21 ustawy
     * 
     */
    MK,

    /**
     * VAT_RR - Faktura VAT RR, o kt�rej mowa w art. 116 ustawy
     * 
     */
    VAT_RR,

    /**
     * WEW - Dokument wewn\u0119trzny
     * 
     */
    WEW;

    public String value() {
        return name();
    }

    public static TDowoduZakupu fromValue(String v) {
        return valueOf(v);
    }

}
