package com.solocorpo

import com.solocorpo.data.ExpensedCarInvoice
import com.solocorpo.data.ExpensedProductInvoice
import com.solocorpo.data.JPKData
import com.solocorpo.xsd.*
import com.solocorpo.xsd.TNaglowek.CelZlozenia
import java.math.BigDecimal
import java.math.BigInteger
import java.time.Instant
import java.util.logging.Level
import java.util.logging.LogRecord
import java.util.logging.Logger
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller
import javax.xml.datatype.DatatypeFactory
import javax.xml.datatype.XMLGregorianCalendar


fun gregorianCalendarFromDateString(date: String): XMLGregorianCalendar =
        DatatypeFactory.newInstance().newXMLGregorianCalendar(date)

fun gregorianCalendarFromNow(): XMLGregorianCalendar =
        DatatypeFactory.newInstance().newXMLGregorianCalendar(Instant.now().toString())

class JPKDataMapperService {

    private val logger: Logger = Logger.getLogger("jpk-generator-kotlin")

    data class Invoice(
            val dataWystawienia: String,
            val nazwa: String,
            val nrDokument: String,
            val NIP: String,
            val index: Int
    )

    fun convertToJPKFile(jpkData: JPKData): JPK {
        logger.log(LogRecord(Level.INFO, "file data received: $jpkData"))
        val jpkBaseObject = JPK()

        jpkBaseObject.deklaracja = buildDeklaracja(jpkData)
        jpkBaseObject.naglowek = buildNaglowek(jpkData.accountingPeriod)
        jpkBaseObject.podmiot1 = buildPodmiot()
        jpkBaseObject.sprzedazWiersz.addAll(buildSprzedaz(jpkData))
        jpkBaseObject.zakupWiersz.addAll(buildZakup(jpkData))
        return jpkBaseObject
    }

    private fun buildZakup(jpkData: JPKData): List<JPK.ZakupWiersz> {
        val invoices = ArrayList<JPK.ZakupWiersz>();
        invoices.addAll(transformExpensedCarInvoices(jpkData.expensedCarInvoices));
        invoices.addAll(transformExpensedProductInvoices(jpkData.expensedProductInvoices));
        return invoices;
    }

    private fun transformExpensedProductInvoices(expensedProductInvoices: List<ExpensedProductInvoice>): List<JPK.ZakupWiersz> {
        return expensedProductInvoices.mapIndexed { index, productInvoice ->
            Invoice(
                    productInvoice.DATA_WYSTAWIENIA,
                    productInvoice.NAZWA,
                    productInvoice.NR_DOKUMENTU,
                    productInvoice.NIP,
                    index + 1
            )
        }.map { invoice -> buildExpenseInvoice(invoice) }
    }

    private fun transformExpensedCarInvoices(expensedCarInvoices: List<ExpensedCarInvoice>): List<JPK.ZakupWiersz> {
        return expensedCarInvoices.mapIndexed { index, carInvoice ->
            Invoice(
                    carInvoice.DATA_WYSTAWIENIA,
                    carInvoice.NAZWA,
                    carInvoice.NR_DOKUMENTU,
                    carInvoice.NIP,
                    index + 1
            )
        }.map { invoice -> buildExpenseInvoice(invoice) }
    };

    private fun buildSprzedaz(jpkData: JPKData): List<JPK.SprzedazWiersz> {
        return jpkData.euIssuedInvoices.mapIndexed{ index, euIssuedInvoice ->
            buildIssuedInvoice(
                    Invoice(
                            euIssuedInvoice.DATA_WYSTAWIENIA,
                            euIssuedInvoice.NAZWA,
                            euIssuedInvoice.NR_DOKUMENTU,
                            euIssuedInvoice.NIP,
                            index + 1
                    )
            )
        }
    }

    private fun buildExpenseInvoice(invoice: Invoice): JPK.ZakupWiersz {
        val zakup = JPK.ZakupWiersz();
        zakup.dataWplywu = gregorianCalendarFromDateString(invoice.dataWystawienia)
        zakup.dataZakupu = gregorianCalendarFromDateString(invoice.dataWystawienia)
        zakup.dokumentZakupu = TDowoduZakupu.VAT_RR
        zakup.dowodZakupu = invoice.nrDokument
        zakup.kodKrajuNadaniaTIN = "PL"
        zakup.lpZakupu = BigInteger.valueOf(invoice.index.toLong())
//                zakup.mpp
//        zakup.nazwaDostawcy
//        zakup.nrDostawcy
//        zakup.zakupVATMarza
        return zakup;
    }

    private fun buildIssuedInvoice(invoice: Invoice): JPK.SprzedazWiersz {
        val euSprzedaz = JPK.SprzedazWiersz()
        euSprzedaz.dataSprzedazy = gregorianCalendarFromDateString(invoice.dataWystawienia)
        euSprzedaz.dataWystawienia = gregorianCalendarFromDateString(invoice.dataWystawienia)
        euSprzedaz.nazwaKontrahenta = invoice.nazwa
        euSprzedaz.dowodSprzedazy = invoice.nrDokument
        euSprzedaz.nrKontrahenta = invoice.NIP
        return euSprzedaz
    }

    private fun buildDeklaracja(jpkData: JPKData): JPK.Deklaracja {
        val deklaracja = JPK.Deklaracja()
        deklaracja.pouczenia = BigDecimal.ONE
        deklaracja.pozycjeSzczegolowe = buildPozycjeSzczegolowe(jpkData)
        return deklaracja
    }

    private fun buildPozycjeSzczegolowe(jpkData: JPKData): JPK.Deklaracja.PozycjeSzczegolowe {
        val pozycjeSzczegolowe = JPK.Deklaracja.PozycjeSzczegolowe()

//        Wysokość podstawy opodatkowania z tytułu dostawy towarów oraz świadczenia usług poza terytorium kraju
        pozycjeSzczegolowe.p11 = BigInteger.valueOf(jpkData.currentPeriodDetails.nettoEUIncome)
//        Wysokość podstawy opodatkowania z tytułu świadczenia usług, o których mowa w art. 100 ust. 1 pkt 4 ustawy
        pozycjeSzczegolowe.p12 = BigInteger.valueOf(jpkData.currentPeriodDetails.nettoEUIncome)

//        Wysokość podstawy opodatkowania z tytułu dostawy towarów oraz świadczenia usług na terytorium kraju, opodatkowanych stawką 22% albo 23%, oraz korekty dokonanej zgodnie z art. 89a ust. 1 i 4 ustawy
        pozycjeSzczegolowe.p19 = BigInteger.valueOf(jpkData.currentPeriodDetails.nettoPLIncome)
//        Wysokość podatku należnego z tytułu dostawy towarów oraz świadczenia usług na terytorium kraju, opodatkowanych stawką 22% albo 23%, oraz korekty dokonanej zgodnie z art. 89a ust. 1 i 4 ustawy
        pozycjeSzczegolowe.p20 = BigInteger.valueOf(jpkData.currentPeriodDetails.vatOwned)

//        Łączna wysokość podstawy opodatkowania. Suma kwot z P_10, P_11, P_13, P_15, P_17, P_19, P_21, P_22, P_23, P_25, P_27, P_29, P_31
        pozycjeSzczegolowe.p37 =
                BigInteger.valueOf(jpkData.currentPeriodDetails.nettoEUIncome + jpkData.currentPeriodDetails.nettoPLIncome)
//        Łączna wysokość podatku należnego. Suma kwot z P_16, P_18, P_20, P_24, P_26, P_28, P_30, P_32, P_33, P_34 pomniejszona o kwotę z P_35 i P_36
        pozycjeSzczegolowe.p38 = BigInteger.valueOf(jpkData.currentPeriodDetails.vatOwned)

//        Wysokość nadwyżki podatku naliczonego nad należnym z poprzedniej deklaracji
        pozycjeSzczegolowe.p39 = BigInteger.valueOf(jpkData.currentPeriodDetails.previousPeriodVATOwned)

//        Wartość netto z tytułu nabycia pozostałych towarów i usług
        pozycjeSzczegolowe.p42 = BigInteger.valueOf(jpkData.currentPeriodDetails.nettoPLExpense)
//        Wysokość podatku naliczonego z tytułu nabycia pozostałych towarów i usług
        pozycjeSzczegolowe.p43 = BigInteger.valueOf(jpkData.currentPeriodDetails.vatExpense)

//        Łączna wysokość podatku naliczonego do odliczenia. Suma kwot z P_39, P_41, P_43, P_44, P_45, P_46 i P_47
        pozycjeSzczegolowe.p48 =
                BigInteger.valueOf(jpkData.currentPeriodDetails.previousPeriodVATOwned + jpkData.currentPeriodDetails.vatExpense)

//        Wysokość podatku podlegająca wpłacie do urzędu skarbowego
        val vatOwned = pozycjeSzczegolowe.p38.subtract(pozycjeSzczegolowe.p48);
        pozycjeSzczegolowe.p51 = if (vatOwned > BigInteger.ZERO) vatOwned else BigInteger.ZERO

//        Wysokość nadwyżki podatku naliczonego nad należnym
        val vatSurplus = pozycjeSzczegolowe.p48.subtract(pozycjeSzczegolowe.p38);
        pozycjeSzczegolowe.p53 = if (vatSurplus >= BigInteger.ZERO) vatSurplus else BigInteger.ZERO

//        Wysokość nadwyżki podatku naliczonego nad należnym do zwrotu na rachunek wskazany przez podatnika
        pozycjeSzczegolowe.p54 = BigInteger.ZERO

//        <!--Zwrot na rachunek rozliczeniowy podatnika w terminie 60 dni-->
//        pozycjeSzczegolowe.p57 = 0

//        Wysokość nadwyżki podatku naliczonego nad należnym do przeniesienia na następny okres rozliczeniowy
        pozycjeSzczegolowe.p62 = pozycjeSzczegolowe.p53

        return pozycjeSzczegolowe
    }

    private fun buildPodmiot(): JPK.Podmiot1 {
        val podmiot = JPK.Podmiot1()
        podmiot.osobaFizyczna = TPodmiotDowolnyBezAdresu.OsobaFizyczna()
        podmiot.rola = "Podatnik"
        podmiot.osobaFizyczna.email = "konieczny93@gmail.com"
        podmiot.osobaFizyczna.telefon = "506466432"
        podmiot.osobaFizyczna.dataUrodzenia = gregorianCalendarFromDateString("1993-01-28")
        podmiot.osobaFizyczna.imiePierwsze = "Bartłomej"
        podmiot.osobaFizyczna.nazwisko = "Konieczny"
        podmiot.osobaFizyczna.nip = "9691622452"
        return podmiot
    }

    private fun buildNaglowek(accountingPeriodString: String): JPK.Naglowek {
        val naglowek = JPK.Naglowek()
        naglowek.celZlozenia = CelZlozenia()
        naglowek.celZlozenia.poz = "P_7"
        naglowek.celZlozenia.value = 1
        naglowek.wariantFormularza = 1
        naglowek.dataWytworzeniaJPK = gregorianCalendarFromNow()
        val kodFormularza = TNaglowek.KodFormularza()
        kodFormularza.wersjaSchemy = "1-0"
        kodFormularza.kodSystemowy = "JPK_V7M (1)"
        kodFormularza.value = TKodFormularza.fromValue("JPK_VAT")
        naglowek.kodFormularza = kodFormularza
        val kodFormularzaDekl = TNaglowek.KodFormularzaDekl()
        kodFormularzaDekl.kodSystemowy = "VAT-7 (21)"
        kodFormularzaDekl.kodPodatku = "VAT"
        kodFormularzaDekl.rodzajZobowiazania = "Z"
        kodFormularzaDekl.wersjaSchemy = "1-0E"
        naglowek.kodFormularzaDekl = kodFormularzaDekl
        naglowek.kodUrzedu = "2413"
        naglowek.nazwaSystemu = "Excel"

        var accountingPeriod = gregorianCalendarFromDateString(accountingPeriodString)
        naglowek.miesiac = accountingPeriod.month.toByte()
        accountingPeriod = gregorianCalendarFromDateString(accountingPeriod.year.toString())
        naglowek.rok = accountingPeriod
        return naglowek
    }

    fun printXML(jpkFile: JPK): Any {
        val jaxbContext: JAXBContext =
                JAXBContext.newInstance(jpkFile::class.java)
        val jaxbMarshaller = jaxbContext.createMarshaller()
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        return jaxbMarshaller.marshal(jpkFile, System.out)
    }
}